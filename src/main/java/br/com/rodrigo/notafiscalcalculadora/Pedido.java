/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rodrigo.notafiscalcalculadora;

/**
 *
 * @author Diamond
 */
public class Pedido {
 private String cliente;
    private double valor;
    private int quantidadeItens;

    public Pedido(String cliente, double valor, int quantidadeItens) {
        this.cliente = cliente;
        this.valor = valor;
        this.quantidadeItens = quantidadeItens;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public int getQuantidadeItens() {
        return quantidadeItens;
    }

    public void setQuantidadeItens(int quantidadeItens) {
        this.quantidadeItens = quantidadeItens;
    }   
}
