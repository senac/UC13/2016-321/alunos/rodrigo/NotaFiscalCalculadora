/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rodrigo.notafiscalcalculadora;

import java.util.Calendar;

/**
 *
 * @author Diamond
 */
public class GeradorDeNotaFiscal {
  private NFDAO dao ;
    private SAP sap ; 
    private ServicoWeb ws ; 

    public GeradorDeNotaFiscal(NFDAO dao , SAP sap ) {
        this.dao = dao;
        this.sap = sap;
    }
    
    
    
    public NotaFiscal GerarNF(Pedido pedido){
        NotaFiscal nf = new NotaFiscal(
                pedido.getCliente()
              , pedido.getValor() * 0.94, 
                Calendar.getInstance()) ;
        
        this.dao.salvar(nf);
        this.sap.envia(nf);
    //    this.ws.call() ; 
        
        
        
        /// enviar pro SAP 
        
        return nf ; 
        
        
    }   
}
